#! /bin/bash -
IFS='   
'
PATH=/bin:/usr/bin
export PATH


## Functions
error()
{
	# Print error, show help and exit
    echo "$@" 1>&2
    usage_exit 1
}

usage_exit()
{
	# Print usage, exit with given exitcode
    usage
    exit "$1"
}

usage()
{
	# Auto adds inputs and outputs from config into help message
    printf "Usage: %s [OPTION]\n
-h --help: Get this message.
-v --version: Get version information.\n
-i= --input=: Set input value
$(readConfigs input 2)
-o= --output=: Set output value
$(readConfigs output 2)\n" "$PROGRAM"
}

version()
{
	# Print program version
    echo "$PROGRAM version $VERSION"
}

readConfigs()
{
	# 1 Exclude comments
	# 2 Print lines that start with given key $1 (input, output, dont_loop)
	# 3 Cut to get label or path $2 (2,3 | key;label;path)
	sed "/^#.*/d" ~/.config/backup-user.config \
		|sed -n "/$1;/p" \
		|cut -d";" -f"$2"
}

isMounted()
{
	# Check that drive absolute path is mounted
	if ! findmnt -rno SOURCE "$output_path" >/dev/null; then
		error "Output path $output_path is not mounted"
	fi
}

isLooping()
{
	# dont_loop - in configs
	# Configurable optional check to make sure input and output devices
	# aren't pointing to the same device
	count=0
	while [ ${count} -lt ${#arrayDontLoop1[@]} ]
	do
		if [ "X$input" = "X${arrayDontLoop1[$count]}" ] \
			&& [ "X$output" = "X${arrayDontLoop2[$count]}" ] \
			|| [ "X$input" = "X${arrayDontLoop2[$count]}" ] \
			&& [ "X$output" = "X${arrayDontLoop1[$count]}" ]
		then
			error "Config file states $input and $output are looping"
		fi
		count=$((count +1))
	done
}

existDirectories()
{
	# Check base output folder exists
	if ! [ -d "$output_path/$backupFolder" ]; then
		mkdir -p "$output_path/$backupFolder/"
	fi

	# Check log output folder exists
	if ! [ -d "$output_path/$logFolder" ]; then
		mkdir -p "$output_path/$logFolder"
	fi
}

getInputPath()
{
	### Combine getInput and getOutput into one function?
	# Check that user given input value matches intput label in configs
	# Get the matching path from configs (key;label;path))
	count=0
	while [ "${count}" -lt "${#arrayInput[@]}" ]
	do
		if [ "X$input" = "X${arrayInput[$count]}" ]; then
			input_path="${arrayInputPath[$count]}"
			break 1
		fi
		count=$((count +1))

		# Give error if value matching label is not found in configs
		[ "${count}" -eq "${#arrayInput[@]}" ] \
			&& error "Input $input value not defined in configs"
	done
}

getOutputPath()
{
	### Combine getInput and getOutput into one function?
	# Check that user given output value matches output label in configs
	# Get the matching path from configs (key;label;path))
	count=0
	while [ "${count}" -lt "${#arrayOutput[@]}" ]
	do
		if [ "X$output" = "X${arrayOutput[$count]}" ]; then
			output_path="${arrayOutputPath[$count]}"
			break 1
		fi
		count=$((count +1))

		# Give error if value matching label is not found in configs
		[ "${count}" -eq "${#arrayOutput[@]}" ] \
			&& error "Output $output value not defined in configs"
	done
}


## Declaring variables
VERSION="1.4.4-1"
PROGRAM=$(basename "$0")
backupFolder=$(readConfigs backupFolder 2)
logFolder=$(readConfigs logFolder 2)
output=
input=
output_path=
input_path=
timestamp="$(date +%Y%m%d-%H%M%S)"
count=

# Config values into arrays
mapfile -t arrayInput< <(readConfigs input 2)
mapfile -t arrayInputPath< <(readConfigs input 3)
mapfile -t arrayOutput< <(readConfigs output 2)
mapfile -t arrayOutputPath< <(readConfigs output 3)
mapfile -t arrayDontLoop1< <(readConfigs dont_loop 2)
mapfile -t arrayDontLoop2< <(readConfigs dont_loop 3)


## Main script

# Get options
while test "$#" -gt 0
do
    case "$1" in
		-h |--help )
			usage_exit 0
			;;
		-v |--version )
			version
			exit 0
			;;
        -i=* |--input=* )
            input="${1#*=}"
            ;;
        -o=* |--output=* )
            output="${1#*=}"
			;;
		* )
			error "Requires proper values for input and output"
			;;
    esac
    shift
done


# Get input values
getInputPath

# Get output values
getOutputPath

# Prevent possible loop of copying to same folder
isLooping

# Make sure output is mounted
isMounted

# Create output folders if not present
existDirectories


# Set default paths if backupFolder and logFolder missing
backupFolder=${backupFolder:-backup}
logFolder=${logFolder:-backup/log}


# Do the backup
rsync -avh --progress --delete \
	--log-file="$output_path/$logFolder/${input}_$timestamp.log" \
	"$input_path" "$output_path/$backupFolder/"
